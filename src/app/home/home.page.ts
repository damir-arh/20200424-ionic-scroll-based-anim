import { Component, ViewChild, ElementRef, ViewChildren, QueryList } from '@angular/core';
import { NavController, IonContent, IonList, IonItem } from '@ionic/angular';

interface ItemAnimationData {
  visible: boolean;
  position: 'above' | 'below';
  offset: number;
}

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  @ViewChild(IonContent, {static: true})
  private ionContent: IonContent;

  @ViewChild(IonList, { static: false, read: ElementRef })
  private ionList: ElementRef;

  @ViewChildren(IonItem, { read: ElementRef })
  private ionItems: QueryList<ElementRef>;

  public readonly items = [
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
    'ten',
    'eleven',
    'twelve',
    'thirteen',
    'fourteen',
    'fifteen',
    'sixteen',
    'seventeen',
    'eighteen',
    'nineteen',
    'twelve'
  ];

  public itemAnimationData: ItemAnimationData[];
  public animationDuration = 200;
  public animationDelay = 70;

  constructor(private navController: NavController) {}

  public ionViewDidLeave() {
    this.itemAnimationData = undefined;
  }

  public pushDetail() {
    this.initializeAnimation().then(() => {
      const maxVisibleAnimationOffset = this.itemAnimationData
        .filter(item => item.visible)
        .map(item => item.offset)
        .reduce((acc, cur) => Math.max(acc, cur));

      setTimeout(() => {
        this.navController.navigateForward('/detail');
      }, maxVisibleAnimationOffset * this.animationDelay + this.animationDuration);
    });
  }

  private initializeAnimation() {
    return this.ionContent.getScrollElement().then(scrollElement => {
      const minYVisible = scrollElement.scrollTop;
      const maxYVisible = scrollElement.scrollTop + scrollElement.offsetHeight;
      const midYVisible = (maxYVisible + minYVisible) / 2;
      const listTop = this.ionList.nativeElement.offsetTop;

      let firstIndexBelow;
      const itemPositions: Pick<ItemAnimationData, 'visible' | 'position'>[] = this.ionItems.map((item, index) => {
        const elementTop = listTop + item.nativeElement.offsetTop;
        const elementBottom = listTop + item.nativeElement.offsetTop + item.nativeElement.offsetHeight / 2;
        const elementMiddle = (elementTop + elementBottom) / 2;
        const visible = elementBottom > minYVisible && elementTop < maxYVisible;
        const position = elementMiddle < midYVisible ? 'above' : 'below';
        if (position === 'below' && firstIndexBelow === undefined) {
          firstIndexBelow = index;
        }
        return { visible, position };
      });

      this.itemAnimationData = itemPositions.map((item, index) => {
        return {
          ...item,
          offset: item.position === 'above' ? firstIndexBelow - index - 1 : index - firstIndexBelow
        };
      });
    });
  }
}
